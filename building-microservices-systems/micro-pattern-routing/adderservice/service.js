module.exports = service

function service() {
  return {
    add(args, callback) {
      const { first, second } = args
      const result = Number.parseInt(first, 10) + Number.parseInt(second, 10)
      callback(null, { result: result.toString() })
    },
  }
}
