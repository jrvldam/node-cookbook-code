const net = require('net')
const nos = require('net-object-stream')
const through = require('through2')
const pump = require('pump')
const bloomrun = require('bloomrun')

const { ADDERSERVICE_SERVICE_PORT = 8080 } = process.env

module.exports = wiring

function wiring(service) {
  const patterns = createPatternRoutes(service)
  const matcher = createMatcherStream(patterns)

  const server = net.createServer(socket => {
    const stream = nos(socket)
    pump(stream, matcher, stream, failure)
  })

  server.listen(ADDERSERVICE_SERVICE_PORT, '0.0.0.0', () => {
    console.log('server listening at', ADDERSERVICE_SERVICE_PORT)
  })
}

function createPatternRoutes(service) {
  const patterns = bloomrun()

  patterns.add({ role: 'adder', cmd: 'add' }, service.add )

  return patterns
}

function createMatcherStream(patterns) {
  return through.obj((object, enc, callback) => {
    const match = patterns.lookup(object)
    if (match === null) {
      return callback()
    }

    match(object, (err, data) => {
      if (err) {
        return callback(null, { status: 'error', err })
      }

      callback(null, data)
    })
  })
}

function failure(err) {
  if (err) {
    return console.error('Server error', err)
  }
  console.error('Stream pipeline ended')
}
