const { Router } = require('express')
const net = require('net')
const nos = require('net-object-stream')

const router = Router()
const {
  ADDERSERVICE_SERVICE_HOST,
  ADDERSERVICE_SERVICE_PORT,
} = process.env

router.get('/', (req, res) => {
  res.render('add', { first: 0, second: 0, result: 0 })
})

router.post('/calculate', (req, res) => {
  const client = createClient('calculate', {
    port: ADDERSERVICE_SERVICE_PORT,
    host: ADDERSERVICE_SERVICE_HOST,
  })
  const role = 'adder'
  const cmd = 'add'
  const { first, second } = req.body

  client
    .once('data', ({ result }) => res.render('add', { first, second, result }))
    .write({ role, cmd, first, second })
})

module.exports = router

function createClient(ns, opts) {
  return createClient[ns] || (createClient[ns] = nos(net.connect(opts)))
}
