const { Router } = require('express')
const restify = require('restify-clients')

const router = Router()
const {
  ADDERSERVICE_SERVICE_HOST,
  ADDERSERVICE_SERVICE_PORT,
} = process.env

router.get('/', (req, res) => {
  res.render('add', { first: 0, second: 0, result: 0 })
})

router.post('/calculate', (req, res, next) => {
  const client = restify.createJSONClient({
    url: `http://${ADDERSERVICE_SERVICE_HOST}:${ADDERSERVICE_SERVICE_PORT}`,
  })
  const { first, second } = req.body

  client.get(`/add/${first}/${second}`, (err, svcReq, svcRes, data) => {
    if (err) {
      return next(err)
    }

    const { result } = data
    res.render('add', { first, second, result })
  })
})

module.exports = router
