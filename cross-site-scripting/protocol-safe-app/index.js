const express = require('express')
const escapeHtml = require('escape-html')

const app = express()

app.get('/', (req, res) => {
  const { prev = '', handoverToken = '', lang = 'en' } = req.query
  pretendDbQuery((err, status) => {
    if (err) {
      return res.sendStatus(500)
    }
    const href = escapeHtml(`${prev}${handoverToken}/${lang}`)
    res.send(`
    <h1>Current Status</h1>
    <div id="stat">
      ${status}
    </div>
    <br>
    <a href="${href}">Back to control HQ</a>
    `)
  })
})

function pretendDbQuery(callback) {
  const status = 'ON FIRE!!! HELP!!!'
  callback(null, status)
}

app.listen(3000)
