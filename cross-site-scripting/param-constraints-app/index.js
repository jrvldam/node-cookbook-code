const express = require('express')

const app = express()

app.get('/', (req, res) => {
  const { prev = '', handoverToken = '', lang = 'en' } = req.query
  if (!validate({ prev, handoverToken, lang }, req.query)) {
    return res.sendStatus(422)
  }

  pretendDbQuery((err, status) => {
    if (err) {
      return res.sendStatus(500)
    }
    res.send(`
    <h1>Current Status</h1>
    <div id="stat">
      ${status}
    </div>
    <br>
    <a href="${prev}${handoverToken}/${lang}">Back to control HQ</a>
    `)
  })
})

function pretendDbQuery(callback) {
  const status = 'ON FIRE!!! HELP!!!'
  callback(null, status)
}

function validate({ prev, handoverToken, lang }, query) {
  let valid = Object.keys(query).length <= 3
  valid = valid && typeof lang === 'string' && lang.length === 2
  valid = valid && typeof handoverToken === 'string' && handoverToken.length === 16
  valid = valid && typeof prev === 'string' && prev.length < 10
  return valid
}

app.listen(3000)
