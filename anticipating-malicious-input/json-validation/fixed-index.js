const { createServer, STATUS_CODES } = require('http')
const Ajv = require('ajv')

const ajv = new Ajv()
const schema = {
  title: 'UserReg',
  properties: {
    id: { type: 'integer' },
    name: { type: 'string' },
    privileges: {
      anyOf: [
        { type: 'string' },
        { type: 'boolean' },
        { type: 'array', items: { type: 'string' } },
        { type: 'object' },
      ],
    },
  },
  additionalProperties: false,
  required: ['id', 'name'],
}
const validate = ajv.compile(schema)

const server = createServer((req, res) => {
  if (req.method !== 'POST') {
    res.statusCode = 404
    return res.end(STATUS_CODES[res.statusCode])
  }
  if (req.url === '/register') {
    return register(req, res)
  }
  res.statusCode = 404
  res.send(STATUS_CODES[res.statusCode])
})

function register(req, res) {
  let data = ''
  req.on('data', chunk => data += chunk)
  req.on('end', () => {
    try {
      data = JSON.parse(data)
    } catch (err) {
      return res.end('{ "ok": false }')
    }
    const valid = validate(data)
    if (!valid) {
      console.error(validate.errors)
      return res.end('{ "ok": false }')
    }
    if (data.hasOwnProperty('privileges')) {
      createAdminUser(data)
      res.end('{ "ok": true, "admin": true }')
    } else {
      createUser(data)
      res.end('{ "ok": true, "admin": false }')
    }
  })
}

function createAdminUser(user) {
  const key = user.id + user.name
}

function createUser(user) {

}

server.listen(3000)
