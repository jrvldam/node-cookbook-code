const http = require('http')

const server = http.createServer((req, res) => {
  if (req.method === 'GET') {
    res.setHeader('Content-Type', 'text/html')
    if (req.url === '/') {
      return res.end(html())
    }
    res.setHeader('Content-Type', 'application/json')
    if (req.url === '/friends') {
      return res.end(friends())
    }
  return
  }
  if (req.method === 'POST') {
    if (req.url === '/') {
      return action(req, res)
    }
  }
})

function html(res) {
  return `
  <div id="friends"></div>
  <form>
    <input id="friend"><input type="submit" value="Add Friend">
  </form>
  <script>
    void function () {
      const friend = document.getElementById('friend')
      const friends = document.getElementById('friends')
      function load() {
        fetch('/friends', {
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
          },
        })
        .catch(console.error)
        .then(response => response.json())
        .then(arr => {
          friends.innerHTML = arr.map(atob).join('<br>')
        })
      }
      
      load()
      
      document.forms[0].addEventListener('submit', () => {
        fetch('/', {
          method: 'POST',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',            
          },
          body: JSON.stringify({ cmd: 'add', friend: friend.value })
        })
      })
      .catch(console.error)
      .then(load)
    }()
  </script>
  `
}

function friends() {
  return JSON.stringify(friends.list)
}
friends.list = [Buffer.from('Dave').toString('base64')]
friends.add = friend => friends.list.push(Buffer.from(friend).toString('base64'))

function action(req, res) {
  let data = ''
  req.on('data', chunk => data += chunk)
  req.on('end', () => {
    try {
      data = JSON.parse(data)
    } catch (err) {
      console.error(err)
      return res.end('{ ok: false }')
    }
    if (data.cmd === 'add') {
      try {
        friends.add(data.friend)
      } catch (err) {
        console.error(err)
        return res.end('{ ok: false }')
      }
    }
    // res.end('{ ok: true }')
  })
}

server.listen(3000)
