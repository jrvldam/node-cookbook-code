const { Router } = require('express')

const router = Router()

router.get('/', (req, res) => {
  const title = 'Express'
  res.render('index', { title })
})

module.exports = router
