const { join } = require('path')
const express = require('express')
const index = require('./routes')

const app = express()
const isDev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 3000

if (isDev) {
  app.use(express.static(join(__dirname, 'public')))
}

app.use('/', index)

app.listen(port, () => console.log(`Server listening on port ${port}`))
