const { join } = require('path')
const express = require('express')
const pino = require('pino')()
const logger = require('express-pino-logger')({ instance: pino })
const session = require('express-session')
const bodyParser = require('body-parser')
const index = require('./routes')
const auth = require('./routes/auth')

const app = express()
const isDev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 3000

app.set('views', join(__dirname, 'views'))
app.set('view engine', 'ejs')

if (isDev) {
  app.use(express.static(join(__dirname, 'public')))
}
if (!isDev) {

  app.set('trust proxy', 1)
}

app.use(logger)
app.use(session({
  secret: 'I like pies',
  resave: false,
  saveUninitialized: false,
  cookie: { secure: !isDev },
}))
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/', index)
app.use('/auth', auth)

app.listen(port, () => pino.info(`Server listening on port ${port}`))
