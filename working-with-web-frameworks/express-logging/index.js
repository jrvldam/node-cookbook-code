const { join } = require('path')
const express = require('express')
const pino = require('pino')()
const logger = require('express-pino-logger')({ instance: pino })
const index = require('./routes')

const app = express()
const isDev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 3000

app.set('views', join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger)

if (isDev) {
  app.use(express.static(join(__dirname, 'public')))
}

app.use('/', index)

app.listen(port, () => pino.info(`Server listening on port ${port}`))
