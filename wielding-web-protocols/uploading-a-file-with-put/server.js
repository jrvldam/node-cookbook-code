const http = require('http')
const fs = require('fs')
const path = require('path')
const through = require('through2')
const pump = require('pump')

const form = fs.readFileSync(path.join(__dirname, 'public', 'index.html'))
const maxSize = 51200

http
  .createServer((req, res) => {
    if (req.method === 'GET') {
      get(res)
      return
    }
    if (req.method === 'PUT') {
      put(req, res)
      return
    }

    reject(405, 'Method Not Allowed', res)
  })
  .listen(8080)

function reject(code, msg, res) {
  res.statusCode = code
  res.end(msg)
}

function get(res) {
  res.writeHead(200, { 'Content-Type': 'text/html' })
  res.end(form)
}

function put(req, res) {
  const size = Number.parseInt(req.headers['content-length'], 10)
  if (Number.isNaN(size)) {
    return reject(400, 'Bad Request', res)
  }

  if (size > maxSize) {
    return reject(413, 'Too Large', res)
  }

  const name = req.headers['x-filename']
  const field = req.headers['x-field']
  const fileName = `${field}-${Date.now()}-${name}`
  const dest = fs.createWriteStream(path.join(__dirname, 'uploads', fileName))
  const counter =  through(function (chunk, enc, cb) {
    this.bytes += chunk.length
    if (this.bytes > maxSize) {
      return cb(Error('size'))
    }
    cb(null, chunk)
  })

  counter.bytes = 0
  counter.on('error', err => {
    if (err.message === 'size') {
      return reject(413, 'Too Large', res)
    }
  })

  pump(req, counter, dest, err => {
    if (err) {
      return reject(500, `Error saving: ${name}!\n`, res)
    }
    res.end(`${name} successfully saved!\n`)
  })
}
