const http = require('http')
const fs = require('fs')
const { Server } = require('ws')

const app = fs.readFileSync('public/index.html')

const server = http.createServer((req, res) => {
  res.setHeader('Content-Type', 'text/html')
  res.end(app)
})

const wss = new Server({ server })

wss.on('connection', socket => {
  socket.on('message', msg => {
    console.log(`Received: ${msg}`)
    if (msg === 'Hello') {
      socket.send('Websockets!')
    }
  })
})

server.listen(8080)
