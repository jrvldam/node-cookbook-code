const fs = require('fs')
const path = require('path')
const smtp = require('smtp-protocol')

const hosts = new Set(['localhost', 'example.com'])
const users = new Set(['you', 'another'])
const mailDir = path.join(__dirname, 'mail')

ensureDir(mailDir)
for (let user of users) {
  ensureDir(path.join(mailDir, user))
}

smtp
  .createServer(req => {
    req.on('to', filter)
    req.on('message', (stream, ack) => save(req, stream, ack))
    req.on('error', console.error)
  })
  .listen(2525)


function ensureDir(dir, callback) {
  try {
    fs.mkdirSync(dir)
  } catch (reason) {
    if (reason.code !== 'EEXIST') {
      throw reason
    }
  }
}

function filter(to, { accept, reject }) {
  const [user, host] = to.split('@')

  if (hosts.has(host) && users.has(user)) {
    accept()
    return
  }
  reject(550, 'mailbox not available')
}

function save(req, stream, { accept }) {
  const { from, to } = req
  accept()
  to.forEach(rcpt => {
    const [user] = rcpt.split('@')
    const dest = path.join(mailDir, user, `${from}-${Date.now()}`)
    const mail = fs.createWriteStream(dest)
    mail.write(`From: ${from} \n`)
    mail.write(`To: ${rcpt} \n\n`)
    stream.pipe(mail, { end: false })
  })
}
