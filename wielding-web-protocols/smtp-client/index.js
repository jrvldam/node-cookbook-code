const os = require('os')
const readLine = require('readline')
const smtp = require('smtp-protocol')

const rl = readLine.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: '',
})
const cfg = {
  host: 'localhost',
  port: 2525,
  mail: 'me@me.com',
  hostname: os.hostname(),
}

rl.on('SIGINT', () => {
  console.log('... cancelled ...')
  process.exit()
})

smtp.connect(cfg.host, cfg.port, mail => {
  mail.helo(cfg.hostname)
  mail.from(cfg.mail)
  rl.question('To: ', to => {
    to.split(/;|,/gm).forEach(rcpt => mail.to(rcpt))
    rl.write('===== Message (^D to send) =====\n')
    mail.data(exitOnFail)
    const body = []
    rl.on('line', line => body.push(`${line}\r\n`))
    rl.on('close', ()=> send(mail, body))
  })
})

function send(mail, body) {
  console.log('... sending ...')
  const message = mail.message()
  body.forEach(message.write, message)
  message.end()
  mail.quit()
}

function exitOnFail(err, code, lines, info) {
  if (code === 550) {
    err = Error(`No Mailbox for Recipient: "${info.rcpt}"`)
  }
  if (!err && ![354, 250, 220, 200].includes(code)) {
    err = Error(`Protocol Error: ${code} ${lines.join(' ')}`)
  }
  if (!err) {
    return
  }
  console.error(err.message)
  process.exit(1)
}
